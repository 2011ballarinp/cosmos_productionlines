


import java.util.*;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CosmosGenerator_2phases {
	
    int numStations;
    int numBuffers; 
    int buffSize [];
    double failProb [];
    double repairProb [];
    int initState [];
    String modelFile  = ""; 
    String propertyFile  = ""; 
	
    public CosmosGenerator_2phases(int numStations, int[] buffSize, double[] failProb,
				  double[] repairProb, int[] initState) {
    	super();
    	if(numStations<=1) {
    		System.out.println("Incorrect number of stations: a production line must contains at least 2 stations");
    		return;
    	}
    	else {
    		this.modelFile += numStations + "M_1phases.gspn";
    		this.numStations = numStations;
    		this.numBuffers = numStations -1 ;
    		this.buffSize = new int[numBuffers]; 
    		this.buffSize = buffSize;
    		this.initState = new int[numStations+numBuffers]; 
    		this.initState = initState;
    		this.failProb = new double[numStations]; 
    		if(failProb.length == numStations && repairProb.length == numStations && buffSize.length == numStations-1) {
    			this.failProb = failProb;
    			this.repairProb = new double[numStations]; 
    			this.repairProb = repairProb;
    		}
    		else 
    			System.out.println("Incorrect length of probability vectors");					
    	}
    }


    public CosmosGenerator_2phases() {
	// TODO Auto-generated constructor stub
    }

    public void setup(String fn) {

	System.out.println("Reading input from "+fn);
	//Path filePath = Paths.get(fn);
	//System.out.println(filePath);
	File file = new File(fn);

	try {
	    Scanner scan = new Scanner(file);  // Create a Scanner object
	    int n=scan.nextInt();  // number of stations
	    double fP [] = new double [n];
	    double rP [] = new double [n];
	    int bS [] = new int [n-1];
	    int iS [] = new int [2*n-1];
	    for(int i=0;i<n;i++)
		fP[i]=scan.nextDouble();
	    for(int i=0;i<n;i++)
		rP[i]=scan.nextDouble();
	    for(int i=0;i<n-1;i++)
	    		bS[i]=scan.nextInt();
	    for(int i=0;i<n;i++)
	    	iS[i]=scan.nextInt();
	    for(int i=0;i<n-1;i++)
			iS[n+i]=scan.nextInt();

	    this.modelFile += fn + ".gspn";
	    this.numStations = n;
	    this.numBuffers = n-1 ;
	    this.buffSize = new int[numBuffers]; 
	    this.buffSize = bS;
	    this.initState = new int[2*n-1]; 
	    this.initState = iS;
	    this.failProb = new double[n]; 	
	    this.failProb = fP;
	    this.repairProb = new double[n]; 
	    this.repairProb = rP;
	} 
	catch (FileNotFoundException e) {
	    e.printStackTrace();
	}
			
    }

    public void generate() {
    	BufferedWriter writer = null;
    	BufferedWriter probMachinesTimedBreakdown [] = new BufferedWriter [this.numStations];
    	BufferedWriter probMachinesTimedRepairAfterBreakdown [] = new BufferedWriter [this.numStations];
    	BufferedWriter probBufferTimedFull [] = new BufferedWriter [this.numBuffers];
    	BufferedWriter probBufferTimedEmptyAfterFull [] = new BufferedWriter [this.numBuffers];
    	
    	
    	try {
    		
    		// generate .gspn file
    		writer = new BufferedWriter( new FileWriter( this.modelFile));
    		writer.write("% GSPN model of production line system\n\n");
    		
    		int i;
    		
    		// generate constants for fail probability
    		writer.write("%% probability machine breakdwon\n");
    		for(i=1;i<= this.numStations;i++) 
    			writer.write("const double p" + i + "=" +  this.failProb[i-1] + ";  % fail probability of machine" + i +" \n");
    		
    		// generate constants for repair probability
    		writer.write("\n%% probability machine gets repaired\n");
    		for(i=1;i<= this.numStations;i++) 
    			writer.write("const double r" + i + "=" +  this.repairProb[i-1] + ";  % repair probability of machine" + i +" \n");
    		
    		// generate constants for buffers capacities
    		writer.write("\n%% buffer capacities\n");
    		writer.write("const int n0=2;  % capacity of fictitious buffer zero is set to 2 \n");    		
    		for(i=1;i< this.numStations;i++) 
    			writer.write("const int n" + i + "=" +  this.buffSize[i-1] + ";  % capacity of buffer " + i +" \n");    		
    		writer.write("const int n" + this.numStations + "=2;  % capacity of fictitious last buffer is set to 2 \n");
    		
    		// generate constants for machines initial-state
    		writer.write("\n%% machines initial states\n");
    		for(i=1;i<= this.numStations;i++) 
    			writer.write("const int M" + i + "_0=" +  this.initState[i-1] + ";  % initial state of machine " + i +" \n");

    		// generate constants for buffers initial-state
    		writer.write("\n%% buffers initial states\n");    		
    		for(i=1;i<=this.numBuffers;i++) 
    			writer.write("const int B" + i + "_0=" +  this.initState[this.numStations+i-1] + ";  % initial state of buffer " + i +" \n");
    	
    		// generate place and transition counters
    		writer.write("\nNbPlaces = " + ((this.numStations*4)+this.numBuffers) + ";\n");
    		writer.write("NbTransitions  = " + this.numStations*6 + ";\n");

    		//generate place list
    		writer.write("\nPlacesList = { ");
    		for(i=1;i<=this.numStations;i++) {
    			writer.write("U" + i +", " + "U" + i +"s, ");
    			writer.write("D" + i +", " + "D" + i +"s, ");
    		}
    		for(i=1;i<=this.numBuffers;i++) 
    			writer.write("B" + i +(i<this.numBuffers? ", " :  ""));	    	
    		writer.write("};\n");
    		
    		// generate transition list
    		writer.write("\nTransitionsList = { ");
    		for(i=1;i<=this.numStations;i++) {
    			writer.write("brk" + i +", " + "nbrk" + i + ", " );
    			writer.write("rep" + i +", " + "nrep" + i + ", ");
    			writer.write("twrk" + i +", " + "tnwrk" + i + ((i<this.numStations)? ", " : ""));
    		}
    		writer.write("};\n");
    		
    		
    		// generate initial marking
    		writer.write("\nMarking = { ");
    		for(i=1;i<=this.numStations;i++) {
    			writer.write("(U" + i +"," + "M" + i + "_0); " );
    			writer.write("(U" + i +"s," + "0); " );
    			writer.write("(D" + i +"," + "1-M" + i + "_0); " );
    			writer.write("(D" + i +"s," + "0); " );
    		}
    		for(i=1;i<=this.numBuffers;i++) 
    			writer.write("(B" + i +"," + "B" + i + "_0); " );
    		writer.write("};\n");

	    	// generate transitions 
    		writer.write("\nTransitions = { ");
    		for(i=1;i<=this.numStations;i++) {
    			writer.write("(brk" + i +",IMMEDIATE,1,p" +  i + "); " );
    	    	writer.write("(nbrk" + i +",IMMEDIATE,1,1-p" +  i + "); " );
    	    	writer.write("(rep" + i +",IMMEDIATE,1,r" +  i + "); " );
    	    	writer.write("(nrep" + i +",IMMEDIATE,1,1-r" +  i + "); " );
    	    	writer.write("(twrk" + i +",DETERMINISTIC(1.0),1,1); " );
    	    	writer.write("(tnwrk" + i +",DETERMINISTIC(1.0),1,1); " );	    	
    	    }
    		writer.write("};\n");
	    	    
    		
    		// generate input arcs for each transition
    		writer.write("\nInArcs = {\n");
    		for(i=1;i<=this.numStations;i++) {
    			if(i==1) {
    	    		writer.write("(U1, brk1, 1); " );
    	    		writer.write("(U1, nbrk1, 1); " );
    	    		writer.write("(D1, rep1, 1); " );
    	    		writer.write("(D1, nrep1, 1); " );
    	    		writer.write("(D1s, tnwrk1, 1); " );
    	    		writer.write("(U1s, twrk1, 1); \n" );
    	    	}
    	    	else if(i>=2 && i<=this.numStations) {
    	    		writer.write("(U" + i + ", brk" +i+ ", 1); " );
    	    		writer.write("(U" + i + ", nbrk" +i+ ", 1); " );
    	    		writer.write("(D" + i + ", rep" +i+ ", 1); " );
    	    		writer.write("(D" + i + ", nrep" +i+ ", 1); " );
    	    		writer.write("(D" + i + "s, tnwrk" +i+ ", 1); " );
    	    		writer.write("(U" + i + "s, twrk" +i+ ", 1); " );
    	    		writer.write("(B" + (i-1) + ", brk" +i+ ", 1); " );
    	    		writer.write("(B" + (i-1) + ", nbrk" +i+ ", 1); " );
    	    		writer.write("(B" + (i-1) + ", twrk" +i+ ", 1); \n" );	    		
    	    	}	    	
    		}
    		writer.write("};\n");
    		
    		// generate output arcs for each transitio
    		writer.write("\nOutArcs = {\n");
    		
    		for(i=1;i<=this.numStations;i++) {
    			writer.write("(brk" + i + ", D" +i+ "s, 1); " );
    	    	writer.write(((i>=2)? "(brk" + i + ", B" +(i-1)+ ", 1); "  : "")) ;
        		writer.write("(nbrk" + i + ", U" +i+ "s, 1); " );
        		writer.write(((i>=2)? "(nbrk" + i + ", B" +(i-1)+ ", 1); "  : "")) ;
        		writer.write("(rep" + i + ", U" +i+ "s, 1); " );
        		writer.write("(nrep" + i + ", D" +i+ "s, 1); " );
        		writer.write("(tnwrk" + i + ", D" +i+ ", 1); " );
        		writer.write("(twrk" + i + ", U" +i+ ", 1); " );
        		writer.write(((i<this.numStations)? "(twrk" + i + ", B" +i+ ", 1);\n"  : "\n")) ;	    	
        	}
    		writer.write("};\n");
	    
	 
    		// generate inhibitor arcs for each transition
    		writer.write("\nInhibitorArcs = {\n");
    		for(i=1;i<this.numStations;i++) {
    			writer.write("(B" + i + ", brk" +i+ ", n" +i+ "); " );
    	    	writer.write("(B" + i + ", nbrk" +i+ ", n" +i+ "); \n");
    	    }
    		writer.write("};\n");
    		
    		for(i=0;i<this.numStations;i++) {
    			
    			// for each machine generate an LHA  that measure the probability that Mi breaksdown within T 
    			probMachinesTimedBreakdown[i] =new BufferedWriter( new FileWriter( "Prob_m" + (i+1) + "Break_withinT.lha"));
    			probMachinesTimedBreakdown[i].write("% probability that machine M" + (i+1)+ " breaks down within T \n\n");
    			probMachinesTimedBreakdown[i].write("NbVariables = 1; \n");
    			probMachinesTimedBreakdown[i].write("NbLocations = 2; \n\n");
    			probMachinesTimedBreakdown[i].write("const double T = 100; \n\n");
    			probMachinesTimedBreakdown[i].write("VariablesList = {time}; \n");
    			probMachinesTimedBreakdown[i].write("LocationsList = {l0, l1}; \n\n");
    			probMachinesTimedBreakdown[i].write("PROB(); \n\n");
    			probMachinesTimedBreakdown[i].write("InitialLocations={l0}; \n");
    			probMachinesTimedBreakdown[i].write("FinalLocations={l1}; \n\n");
    			probMachinesTimedBreakdown[i].write("Locations={\n"
    					+ "(l0, D" +(i+1)+ "<=0 , (time:1));\n"
    					+ "(l1, D" +(i+1)+ ">=1, (time:1));\n"
    					+ "}; \n\n");
    			probMachinesTimedBreakdown[i].write("Edges={\n"
    					+ "((l0,l0),ALL,  time<=T,#);\n"
    					+ "((l0,l1),ALL,  time<=T,{time=0});\n"
    					+ "};\n\n");
    			
    			// for each machine generate an LHA  that measure the probability that Mi breaksdown within T and then gets repaired within T1
    			probMachinesTimedRepairAfterBreakdown[i] = new BufferedWriter( new FileWriter( "Prob_m" + (i+1) + "Break_withinT_thenRepaired_withinT1.lha")); //"RepairAfterBreakDown.lha"));
    			probMachinesTimedRepairAfterBreakdown[i].write("% probability that machine M" + (i+1)+ " breaks down within T and then gets repaired within T1 \n\n");
    			probMachinesTimedRepairAfterBreakdown[i].write("NbVariables = 1; \n");
    			probMachinesTimedRepairAfterBreakdown[i].write("NbLocations = 3; \n\n");
    			probMachinesTimedRepairAfterBreakdown[i].write("const double T = 100; \n");
    			probMachinesTimedRepairAfterBreakdown[i].write("const double T1 = 10; \n\n");
    			probMachinesTimedRepairAfterBreakdown[i].write("VariablesList = {time}; \n");
    			probMachinesTimedRepairAfterBreakdown[i].write("LocationsList = {l0, l1, l2}; \n\n");
    			probMachinesTimedRepairAfterBreakdown[i].write("PROB(); \n\n");
    			probMachinesTimedRepairAfterBreakdown[i].write("InitialLocations={l0}; \n");
    			probMachinesTimedRepairAfterBreakdown[i].write("FinalLocations={l2}; \n\n");
    			probMachinesTimedRepairAfterBreakdown[i].write("Locations={\n"
    					+ "(l0, D" +(i+1)+ "<=0 , (time:1));\n"
    					+ "(l1, D" +(i+1)+ ">=1 | U" + (i+1)+ "<=0, (time:1));\n"
    					+ "(l2, U" +(i+1)+ ">=1 , (time:1));\n"
    					+ "}; \n\n");
    			probMachinesTimedRepairAfterBreakdown[i].write("Edges={\n"
    					+ "((l0,l0),ALL,  time<=T,#);\n"
    					+ "((l0,l1),ALL,  time<=T,{time=0});\n"
    					+ "((l1,l1),ALL,  time<=T1,#);\n"
    					+ "((l1,l2),ALL,  time<=T1,{time=0});\n"
    					+ "};\n\n");
    		}
    		
    		for(i=0;i<this.numBuffers;i++) {
    			// for each buffer generate an LHA  that measure the probability that Bi gets full within T 
    			probBufferTimedFull[i] =new BufferedWriter( new FileWriter( "Prob_b" + (i+1) + "Full_withinT.lha"));
    			probBufferTimedFull[i].write("% probability that buffer B" + (i+1)+ " gets full within T \n\n");
    			probBufferTimedFull[i].write("NbVariables = 1; \n");
    			probBufferTimedFull[i].write("NbLocations = 2; \n\n");
    			probBufferTimedFull[i].write("const double T = 100; \n\n");
    			probBufferTimedFull[i].write("const int N = " +this.buffSize[i]+ "; % size of buffer \n\n");
    			probBufferTimedFull[i].write("VariablesList = {time}; \n");
    			probBufferTimedFull[i].write("LocationsList = {l0, l1}; \n\n");
    			probBufferTimedFull[i].write("PROB(); \n\n");
    			probBufferTimedFull[i].write("InitialLocations={l0}; \n");
    			probBufferTimedFull[i].write("FinalLocations={l1}; \n\n");
    			probBufferTimedFull[i].write("Locations={\n"
    					+ "(l0, B" +(i+1)+ "<N , (time:1));\n"
    					+ "(l1, B" +(i+1)+ "=N, (time:1));\n"
    					+ "}; \n\n");
    			probBufferTimedFull[i].write("Edges={\n"
    					+ "((l0,l0),ALL,  time<=T,#);\n"
    					+ "((l0,l1),ALL,  time<=T,{time=0});\n"
    					+ "};\n\n");
    			
    			// for each machine generate an LHA  that measure the probability that Mi breaksdown within T and then gets repaired within T1
    			probBufferTimedEmptyAfterFull[i] = new BufferedWriter( new FileWriter( "Prob_b" + (i+1) + "Full_withinT_thenEmpty_withinT1.lha"));
    			probBufferTimedEmptyAfterFull[i].write("% probability that machine B" + (i+1)+ " gets full within T and then gets empty within T1 \n\n");
    			probBufferTimedEmptyAfterFull[i].write("NbVariables = 1; \n");
    			probBufferTimedEmptyAfterFull[i].write("NbLocations = 3; \n\n");
    			probBufferTimedEmptyAfterFull[i].write("const double T = 100; \n");
    			probBufferTimedEmptyAfterFull[i].write("const double T1 = 10; \n\n");
    			probBufferTimedEmptyAfterFull[i].write("const int N = " +this.buffSize[i]+ "; % size of buffer \n\n");
    			probBufferTimedEmptyAfterFull[i].write("VariablesList = {time}; \n");
    			probBufferTimedEmptyAfterFull[i].write("LocationsList = {l0, l1, l2}; \n\n");
    			probBufferTimedEmptyAfterFull[i].write("PROB(); \n\n");
    			probBufferTimedEmptyAfterFull[i].write("InitialLocations={l0}; \n");
    			probBufferTimedEmptyAfterFull[i].write("FinalLocations={l2}; \n\n");
    			probBufferTimedEmptyAfterFull[i].write("Locations={\n"
    					+ "(l0, B" +(i+1)+ "<N , (time:1));\n"
    					+ "(l1, B" +(i+1)+ "=N , (time:1));\n"
    					+ "(l2, B" +(i+1)+ "=0 , (time:1));\n"
    					+ "}; \n\n");
    			probBufferTimedEmptyAfterFull[i].write("Edges={\n"
    					+ "((l0,l0),ALL,  time<=T,#);\n"
    					+ "((l0,l1),ALL,  time<=T,{time=0});\n"
    					+ "((l1,l1),ALL,  time<=T1,#);\n"
    					+ "((l1,l2),ALL,  time<=T1,{time=0});\n"
    					+ "};\n\n");
    	
    		}
	   
		} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	 }
    	finally
	    {
		try
		    {
		        if ( writer != null)
		        	writer.close( );
		        for(int i=0;i<this.numStations;i++) {
		        	if ( probMachinesTimedBreakdown[i] != null)
		        		probMachinesTimedBreakdown[i].close();
		        	if ( probMachinesTimedRepairAfterBreakdown[i] != null)
		        		probMachinesTimedRepairAfterBreakdown[i].close();		        	
		        }
		        
		        for(int i=0;i<this.numBuffers;i++) {		    
		        	if ( probBufferTimedFull[i] != null)
		        		probBufferTimedFull[i].close();
		        	if ( probBufferTimedEmptyAfterFull[i] != null)
		        		probBufferTimedEmptyAfterFull[i].close();
		        }
		    }
		catch (IOException e)
		    {
		    }
	    }

		
    }

    @Override
    public String toString() {
	return "PRISMGenerator [numStations=" + numStations + ", numBuffers=" + numBuffers + ", buffSize="
	    + Arrays.toString(buffSize) + ", failProb=" + Arrays.toString(failProb) + ", repairProb="
	    + Arrays.toString(repairProb) + ", initState(machines, buffers)=" + Arrays.toString(initState) +
	    ", outFile=" + modelFile + "]";
    }


    public static void main(String[] args) {		
    	CosmosGenerator_2phases g = new CosmosGenerator_2phases();
    	g.setup(args[0]);
    	System.out.println(g);
    	g.generate();		
    }
}
